# 8 10 Docker



## Task
1. Review all config files, dockerfile, docker-compose.yml and find any errors. For example, a dockerfile may not have the FROM nginx:alpine/ layer as a source for the image; other dockerfiles may not have volume /data, port 6379, users.
2. Troubleshoot with possible debugging and get all the components of a fully working software consisting of nginx, redis, mysql.

# Result
```
❯ docker ps
CONTAINER ID   IMAGE                                               COMMAND                  CREATED         STATUS         PORTS                                                                          NAMES
6657745d5d2c   docker-nginx-python2-db-redis-homework_nginx_todo   "/docker-entrypoint.…"   6 seconds ago   Up 5 seconds   0.0.0.0:443->443/tcp, :::443->443/tcp, 0.0.0.0:8080->80/tcp, :::8080->80/tcp   docker-nginx-python2-db-redis-homework_nginx_todo_1
ce880306cb91   docker-nginx-python2-db-redis-homework_consumers    "/usr/bin/supervisor…"   7 seconds ago   Up 5 seconds   9000/tcp                                                                       docker-nginx-python2-db-redis-homework_consumers_1
f3fbcfb05097   docker-nginx-python2-db-redis-homework_todo         "uwsgi"                  7 seconds ago   Up 5 seconds   9000/tcp                                                                       docker-nginx-python2-db-redis-homework_todo_1
b8921b4b63e9   docker-nginx-python2-db-redis-homework_redis        "docker-entrypoint.s…"   7 seconds ago   Up 6 seconds   0.0.0.0:6379->6379/tcp, :::6379->6379/tcp                                      docker-nginx-python2-db-redis-homework_redis_1
affc70de5047   docker-nginx-python2-db-redis-homework_db           "docker-entrypoint.s…"   7 seconds ago   Up 6 seconds   0.0.0.0:3306->3306/tcp, :::3306->3306/tcp, 33060/tcp                           docker-nginx-python2-db-redis-homework_db_1

❯ docker-compose ps
         Name                   Command           State           Ports        
--------------------------------------------------------------------------------
docker-nginx-            /usr/bin/supervisord     Up      9000/tcp              
python2-db-redis-        -c /e ...                                              
homework_consumers_1                                                            
docker-nginx-            docker-entrypoint.sh     Up      0.0.0.0:3306->3306/tcp
python2-db-redis-        mysqld                           ,:::3306->3306/tcp,  
homework_db_1                                             33060/tcp            
docker-nginx-            /docker-entrypoint.sh    Up      0.0.0.0:443->443/tcp,:
python2-db-redis-        /bin ...                         ::443->443/tcp, 0.0.0.
homework_nginx_todo_1                                     0:8080->80/tcp,:::8080
                                                          ->80/tcp              
docker-nginx-            docker-entrypoint.sh     Up      0.0.0.0:6379->6379/tcp
python2-db-redis-        redis ...                        ,:::6379->6379/tcp    
homework_redis_1                                                                
docker-nginx-            uwsgi                    Up      9000/tcp              
python2-db-redis-                                                              
homework_todo_1                                                                
 /home/system/skillbox/docker-nginx-python2-db-redis-homework
 ```