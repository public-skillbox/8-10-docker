FROM python:3.9-bullseye

# Install supervisor
RUN apt-get update && apt-get install -y supervisor && \
    rm -rf /var/lib/apt/lists/*

# uWSGI:
RUN pip install uwsgi

# Ensure the directory for uWSGI plugins exists and set permissions for uWSGI log directory
RUN mkdir -p /usr/lib/uwsgi/plugins/ && chown www-data:www-data /usr/lib/uwsgi/plugins/ && \
    mkdir -p /var/log/uwsgi/ && chown www-data:www-data /var/log/uwsgi/

# Make sure the conf.d directory exists for included supervisor configurations
RUN mkdir -p /etc/supervisor/conf.d

# Copy over the application's requirements and install them
COPY ./requirements.txt /opt/
RUN pip install -r /opt/requirements.txt

# Copy the base uWSGI ini file to enable default dynamic uWSGI process number
COPY ./uwsgi.ini /etc/uwsgi/
ENV UWSGI_INI /etc/uwsgi/uwsgi.ini
ENV UWSGI_CHEAPER 2
ENV UWSGI_PROCESSES 16

# Set the work directory and user for the application
ARG WORK_DIR=/var/www
WORKDIR ${WORK_DIR}


# Create necessary directories for logging
RUN mkdir -p ${WORK_DIR}/todo/logs && \
    chown -R www-data:www-data ${WORK_DIR}/todo/logs
RUN usermod -u 1000 www-data


# Additional installations based on your application's needs can go here...
# (CRON, NPM, GIT, ZIP, Image optimizers, pgsql client installations...)

# Copy supervisor configuration (make sure this file is properly configured to manage uWSGI and other processes)
COPY ./supervisor/supervisord.conf /etc/supervisor/

# Specify the command to run supervisord
CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/supervisord.conf"]

EXPOSE 9000
